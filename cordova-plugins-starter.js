angular.module('cordova.plugins.starter', [])

.run(function(Toast, Push, Kakao, MobileAccessibility) {
  Toast.init();
  Kakao.init();
  MobileAccessibility.init();
})

.factory('MobileAccessibility', function() {
  return {
    init: function() {
      if(window.cordova) {
        document.addEventListener('deviceready', function() {
          if(MobileAccessibility) {
            // 텍스트 줌 설정 비활성화
            MobileAccessibility.usePreferredTextZoom(false);  
          }
        });
      }
    }
  }
})


.factory('Toast', function() {
  return {
    init: function() {
      if(window.cordova) {
        alert = function(message) {
          document.addEventListener('deviceready', function() {
            window.plugins.toast.showWithOptions(
              {
                message: message,
                duration: 'short',
                position: 'center',
                addPixelsY: 0
              },
              function() {
                // onSuccess callback
              }, 
              function() {
                // onError callback
              }
            );
          });
        }
      }
    }
  }
})

.factory('Spinner', function() {
  return {
    show: function(title, message, callback) {
      if(window.cordova) {
        document.addEventListener('deviceready', function() {
          if(device.platform == 'iOS') {
            if(title && message) {
              ProgressIndicator.showSimpleWithLabelDetail(callback, title, message);
            } else if(title) {
              ProgressIndicator.showSimpleWithLabel(callback, title);
            } else {
              ProgressIndicator.showSimple(callback);
            }
          } else {
            window.plugins.spinnerDialog.show(title, message, callback);  
          }
        });
      }
    },
    hide: function() {
      if(window.cordova) {
        document.addEventListener('deviceready', function() {
          if(device.platform == 'iOS') {
            ProgressIndicator.hide();
          } else {
            window.plugins.spinnerDialog.hide();
          }
        });
      }
    }
  }
})

.factory('ActionSheet', function() {
  return function(options, callback) {
    if(window.cordova) {
      document.addEventListener('deviceready', function() {
        window.plugins.actionsheet.show(options, callback);
      });  
    } else {
      alert('ActionSheet 를 지원하지 않는 환경입니다');
    }
  }
})

.factory('Push', function(Config) {
  return {
    init: function(registrationCallback, notificationCallback, errorCallback) {
      document.addEventListener('deviceready', function() {
        var push = PushNotification.init({
          'android': {'senderID': Config.gcmSenderId},
          'ios': {'alert': 'true', 'badge': 'true', 'sound': 'true'},
          'windows': {}
        });
        push.on('registration', function(data) {
          if(registrationCallback) registrationCallback(data);
        });
        push.on('notification', function(data) {
          if(notificationCallback) notificationCallback(data);
          // data.message,
          // data.title,
          // data.count,
          // data.sound,
          // data.image,
          // data.additionalData
        });
        push.on('error', function(e) {
          if(errorCallback) errorCallback(e);
          // e.message
        });
      });
    }
  }
})

.factory('Facebook', function() {
  return {
    login: function(callback) {
      if(window.cordova) {
        // Native Login
        document.addEventListener('deviceready', function() {
          facebookConnectPlugin.login(['public_profile'], function(response) {
            facebookConnectPlugin.api('/me/?fields=id,name,picture.type(large)', function(response) {
              if(response.id) {
                callback({
                  id: response.id,
                  name: response.name,
                  picture: response.picture.data.url
                });
              }
            }, function(response) {
              // 분명히 성공했는데 Error Callback 으로 떨어지는 기이한 현상;;
              // 어쨌든 Response 가 있으면 Success 로 간주하고 처리합니다.
              if(response.id) {
                callback({
                  id: response.id,
                  name: response.name,
                  picture: response.picture.data.url
                });
              }
            });
          }, function(response) {
            alert('로그인 하지 못했습니다');
          });
        });
      } else {
        alert('페이스북 로그인을 지원하지 않는 환경입니다');
      }
    },
    logout: function(callback) {
      //
    }
  }
})

.factory('Kakao', function(Config) {
  return {
    init: function() {
      // Javascript App init.
      if(!window.cordova) {
        Kakao.init(Config.kakaoAppKeyJavascript);
      }
    },
    login: function(callback) {
      if(window.cordova) {
        // Native Login
        document.addEventListener('deviceready', function() {
          KakaoTalk.login(
            function (result) {
              callback(result);
            },
            function (message) {
              alert('로그인 하지 못했습니다');
            }
          );
        });
      } else {
        // Javascript Logins
        Kakao.Auth.login({
          success: function(authObj) {
            // 로그인 성공시, API를 호출합니다.
            Kakao.API.request({
              url: '/v1/user/me',
              success: function(result) {
                var user = {id: result.id};
                angular.forEach(result.properties, function(value, key) {
                  user[key] = value;
                });
                callback(user);
              },
              fail: function(error) {
                alert('카카오 프로필을 가져오지 못했습니다');
              }
            });
          },
          fail: function(err) {
            alert('로그인 하지 못했습니다');
          }
        });  
      }
    },
    logout: function() {
      //
    }
  }
})

.factory('Browser', function() {
  return function(options) {
    // Google Analytics Tracking
    if(window.ga) ga('send', 'pageview', options.url);
    // 
    if(window.cordova == undefined) {
      window.open(options.url,'_blank');
      return false;
    }
    document.addEventListener('deviceready', function() {
      var ref = cordova.ThemeableBrowser.open(options.url, options.target, {
        transitionstyle: (options.transitionstyle ? options.transitionstyle : 'coververtical'),
        statusbar: {
            color: options.bgcolor
        },
        toolbar: {
            height: 44,
            color: options.bgcolor
        },
        title: function() {
          if(options.title) {
            return {
              color: options.textcolor,
              staticText: options.title
            };
          } else {
            return {
              color: options.textcolor,
              showPageTitle: true
            };
          }
        }(),
        backButton: (function() {
          if(options.navigation) {
            return {
                wwwImage: 'lib/cordova-plugins-starter/btn_prev.png',
                wwwImagePressed: 'lib/cordova-plugins-starter/btn_prev_active.png',
                wwwImageDensity: (function() {
                  if(device.platform.toLowerCase() == 'ios')
                    return 3;
                  else if(device.platform.toLowerCase() == 'android')
                    return 4;
                })(),
                align: 'left',
                event: 'backPressed'
            }
          }
        })(),
        forwardButton: (function() {
          if(options.navigation) {
            return {
                wwwImage: 'lib/cordova-plugins-starter/btn_next.png',
                wwwImagePressed: 'lib/cordova-plugins-starter/btn_next_active.png',
                wwwImageDensity: (function() {
                  if(device.platform.toLowerCase() == 'ios')
                    return 3;
                  else if(device.platform.toLowerCase() == 'android')
                    return 4;
                })(),
                align: 'left',
                event: 'forwardPressed'
            }
          }
        })(),
        closeButton: {
          wwwImage: 'lib/cordova-plugins-starter/btn_close.png',
          wwwImagePressed: 'lib/cordova-plugins-starter/btn_close_active.png',
          wwwImageDensity: (function() {
            if(device.platform.toLowerCase() == 'ios')
              return 3;
            else if(device.platform.toLowerCase() == 'android')
              return 4;
          })(),
          align: 'left',
          event: 'closePressed'
        },
        customButtons: [
          {
            wwwImage: 'lib/cordova-plugins-starter/btn_web.png',
            wwwImagePressed: 'lib/cordova-plugins-starter/btn_web_active.png',
            wwwImageDensity: (function() {
              if(device.platform.toLowerCase() == 'ios')
                return 3;
              else if(device.platform.toLowerCase() == 'android')
                return 4;
            })(),
            align: 'right',
            event: 'customPressed'
          }
        ],
        // menu: {
        //     image: 'menu',
        //     imagePressed: 'menu_pressed',
        //     title: 'Test',
        //     cancel: 'Cancel',
        //     align: 'right',
        //     items: [
        //         {
        //             event: 'helloPressed',
        //             label: 'Hello World!'
        //         },
        //         {
        //             event: 'testPressed',
        //             label: 'Test!'
        //         }
        //     ]
        // },
        backButtonCanClose: true
      }).addEventListener('backPressed', function(e) {
        // alert('back pressed');
      }).addEventListener('helloPressed', function(e) {
        // alert('hello pressed');
      }).addEventListener('customPressed', function(e) {
        cordova.ThemeableBrowser.open(options.url, '_system');
      }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(e) {
        console.error(e.message);
      }).addEventListener(cordova.ThemeableBrowser.EVT_WRN, function(e) {
        console.log(e.message);
      })  
      .addEventListener('loadstop', function() {
        if(options.script)
          ref.executeScript({code: options.script});
      });
    });
  };
});




